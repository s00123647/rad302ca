﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CA1.Models;
using CA1.DAL;

namespace CA1.Controllers
{
    public class LegsController : Controller
    {
        private tourContext db = new tourContext();

        //
        // GET: /Legs/

        public ActionResult Index()
        {
            return View(db.Legs.ToList());
        }

        //
        // GET: /Legs/Details/5

        public ActionResult Details(int id = 0)
        {
            Legs legs = db.Legs.Find(id);
            if (legs == null)
            {
                return HttpNotFound();
            }
            return View(legs);
        }

        //
        // GET: /Legs/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Legs/Create

        [HttpPost]
        public ActionResult Create(Legs legs)
        {
            if (ModelState.IsValid)
            {
                db.Legs.Add(legs);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(legs);
        }

        //
        // GET: /Legs/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Legs legs = db.Legs.Find(id);
            if (legs == null)
            {
                return HttpNotFound();
            }
            return View(legs);
        }

        //
        // POST: /Legs/Edit/5

        [HttpPost]
        public ActionResult Edit(Legs legs)
        {
            if (ModelState.IsValid)
            {
                db.Entry(legs).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(legs);
        }

        //
        // GET: /Legs/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Legs legs = db.Legs.Find(id);
            if (legs == null)
            {
                return HttpNotFound();
            }
            return View(legs);
        }

        //
        // POST: /Legs/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Legs legs = db.Legs.Find(id);
            db.Legs.Remove(legs);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace CA1.Models
{
   public class Trip
    {
       
        public int tripID { get; set; }
        public string name { get; set; }

        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public bool Viable { get; set; }
        public bool NotViable { get; set; }
     
        public ICollection<Legs> no_ofLegs { get; set; }
        
    }
   public class Legs
    {

       [Key]
        public int legID { get; set; }
        public string startLocal { get; set; }
        public string finishLocal { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }

        public ICollection<Guest> guest { get; set; }

    }
   public class Guest
    {
        public int guestID { get; set; }
        public string fName { get; set; }

    }
}

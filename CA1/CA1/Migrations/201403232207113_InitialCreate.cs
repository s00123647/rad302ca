namespace CA1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Trip",
                c => new
                    {
                        tripID = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        startDate = c.DateTime(nullable: false),
                        endDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.tripID);
            
            CreateTable(
                "dbo.Legs",
                c => new
                    {
                        legID = c.Int(nullable: false, identity: true),
                        startLocal = c.String(),
                        finishLocal = c.String(),
                        startDate = c.DateTime(nullable: false),
                        endDate = c.DateTime(nullable: false),
                        Trip_tripID = c.Int(),
                    })
                .PrimaryKey(t => t.legID)
                .ForeignKey("dbo.Trip", t => t.Trip_tripID)
                .Index(t => t.Trip_tripID);
            
            CreateTable(
                "dbo.Guest",
                c => new
                    {
                        guestID = c.Int(nullable: false, identity: true),
                        fName = c.String(),
                        Legs_legID = c.Int(),
                    })
                .PrimaryKey(t => t.guestID)
                .ForeignKey("dbo.Legs", t => t.Legs_legID)
                .Index(t => t.Legs_legID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Guest", new[] { "Legs_legID" });
            DropIndex("dbo.Legs", new[] { "Trip_tripID" });
            DropForeignKey("dbo.Guest", "Legs_legID", "dbo.Legs");
            DropForeignKey("dbo.Legs", "Trip_tripID", "dbo.Trip");
            DropTable("dbo.Guest");
            DropTable("dbo.Legs");
            DropTable("dbo.Trip");
        }
    }
}

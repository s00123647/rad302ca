namespace CA1.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using CA1.Models;
    internal sealed class Configuration : DbMigrationsConfiguration<CA1.DAL.tourContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CA1.DAL.tourContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Guests.AddOrUpdate(
                g => g.fName,
                new Guest { guestID = 1, fName = "Mary" },
                new Guest { guestID = 2, fName = "Martin" },
                new Guest { guestID = 3, fName = "Margret" }
                
                );

            context.Trip.AddOrUpdate(
                t => t.name,
               new Trip { tripID = 1, name = "curry tour", startDate = DateTime.Parse("12/1/2014"), endDate = DateTime.Parse("17/1/2014") }

                );

        


        }

    }
}

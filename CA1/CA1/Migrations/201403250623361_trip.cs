namespace CA1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class trip : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trip", "Viable", c => c.Boolean(nullable: false));
            AddColumn("dbo.Trip", "NotViable", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Trip", "NotViable");
            DropColumn("dbo.Trip", "Viable");
        }
    }
}
